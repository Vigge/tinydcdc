cmake_minimum_required(VERSION 3.14)
SET(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_CXX_STANDARD 14)

# Sourcing the avr headers absolute path may be needed on some systems:
#SET(CMAKE_FIND_ROOT_PATH /usr/lib/gcc/avr )

#SET(CMAKE_C_COMPILER avr-gcc)
SET(CMAKE_CXX_COMPILER avr-g++)

# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

#SET(CSTANDARD "-std=gnu99")
#SET(CTUNING "-funsigned-char -funsigned-bitfields -fpack-struct -fshort-enums")
SET(CDEBUG "-gstabs")
SET(CWARN "-Wall -Wstrict-prototypes")
SET(COPT "-Os -fno-threadsafe-statics")
SET(CMCU "-mmcu=attiny85")
SET(CDEFS "-DF_CPU=16000000")


SET(CFLAGS "${CMCU} ${CDEBUG} ${CDEFS} ${CINCS} ${COPT} ${CWARN} ${CSTANDARD} ${CEXTRA}")
SET(CXXFLAGS "${CMCU} ${CDEFS} ${CINCS} ${COPT}")

SET(CMAKE_C_FLAGS  ${CFLAGS})
SET(CMAKE_CXX_FLAGS ${CXXFLAGS})

add_executable(tinyDcDc.elf
        main.cpp
        pwm.cpp
        pwm.h
        adc.cpp
        adc.h
        pidDcDc.cpp
        pidDcDc.h
        spislaveusi.cpp
        spislaveusi.h
        ssfir.cpp
        ssfir.h)
