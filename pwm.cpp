#include "pwm.h"
#include <util/delay.h>


/***************************************************
 * Sets the switching frequency                    *
 *                                                 *
 * - if initPll is used:                           *
 * PCK:       64e6 / 2**8           = 250 KHz      *
 * PCK_16384: 64e6 / 16384 / 2**8  ~= 15.3 Hz      *
 *                                                 *
 * - if a 16MHz clock source is used               *
 * PCK:       16e6 / 2**8           = 62.5 KHz     *
 * PCK_16384: 16e6 / 16384 / 2**8  ~= 3.8 Hz       *
 **************************************************/
static inline void setSwitchFreq()
{
    // max freq, no clock divider: PCK
    TCCR1 = (TCCR1 & 0b11110000) | Pwm::FREQ::PCK;

}

/********************************
 * Inits the PLL in fast mode.  *
 * - 64 MHz                     *
 *******************************/
static inline void initPll()
{
    // PLLCSR: LSM - - - - PCKE PLLE PLOCK
    // 1. enable PLL
    PLLCSR |= 1 << PLLE;

    // 2. Give the PLL some time to stabilise, then wait for lock
    _delay_us(100);
    while (true)
    {
        if (PLLCSR & 1 > 0)
            break;
    }

    // 3. Enable the PLL
    PLLCSR |= 1 << PCKE;

}

Pwm::Pwm()
{
    /************************
     * Pwm freq init        *
     * - 250KHz switch freq *
     ***********************/
    initPll();
    setSwitchFreq();

    /*************************************************************
     * Hard enable of pwm and pins                               *
     * - Change these values if other pwm pins etc are preferred *
     ************************************************************/
    // PWM OC1A on:
//    TCCR1 |= 0b01010000; // !CTC1 PWM1A !COM1A1 COM1A0 !CS13 !CS12 !CS11 !CS10

    // OC1A uses PB0 and PB1
//    DDRB |= (1 << PB0) | (1 << PB1);

    // PWM OC1B on:
    GTCCR |= 0b01010000; // !TSM PWM1B !COM1B1 COM1B0 !FOC1B !FOC1A !PSR1 !PSR0

    // OC1B uses PB3 and PB4
//    DDRB |= (1 << PB3) | (1 << PB4);
    DDRB |= (1 << PB3);

}

void Pwm::setPwmRatio(char ratioBytes)
{
//    OCR1A = ratioBytes;   // pwm ratio for PWM1A
    OCR1B = ratioBytes;     // pwm ratio for PWM1B
}