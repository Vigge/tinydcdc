# TinyDcDC
\- A small spi controlled DC/DC module based on Attiny85 
(Atmel AVR)

Features:
* Voltage set via SPI (from raspberry pi etc.) 
* 8 bits of programmable voltage levels
* PID regulated voltage loop
* 250 KHz 8 bit PWM switching frequency

## Hardware and wiring instructions:

### ATTiny85

| Package PIN  | AVR Pin     | Description            | 
|--------------|-------------|------------------------|
|      5       | PB0         | to be MOSI             |
|      6       | PB1         | loop speed ind. (to be MISO)|
|      7       | PB2         | to be SCK              |
|      2       | PB3 / !OC1B | PWM out                |
|      3       | PB4         | ADC / Voltage feedback |
|      1       | Reset       | Reset                  |
|      4       | GND         | GND                    |
|      8       | VCC         | VCC                    |

### Raspberry Pi
If connecting the TinyDcDc to a rasberry pi:

| ATTiny85 pin | Raspberry GPIO |
|--------------|----------------|
| 5 MOSI       |  10 MOSI       |
| 6 MISO       |  09 MISO       |
| 7 SCK        |  11 CLK        |
| 4 GND        |  GND           |

=============
= **Note!** =
=============

The PI GPIO cannot source/sink more than about 15mA. Connect the 
spi pins using resistors if the Attiny85 and Pi are fed by 
different power sources or you may fry the Pi GPIO!

- 1kohm should work fine if using a 5V powered ATTiny.


#### Testing on Raspberry Pi
Once the pi is connected you can set the tinyDcDC target voltage 
with the following example:

```bash
#!/bin/bash -xe

TARGET_VOLT=127 # 0 to 255

pigs spio 0 32000 0  # Open a pigs spi device as handle 0
# Note, if this fails you probably don't have pigpiod running,
# it can be started by running "sudo pigpiod"

echo "Check chip ID:"
pigs spix 0 0x81 0  
# Expect 49 back or something is not working correctly:
# > pi@raspberrypi:~ $ pigs spix 0 0x81 0
# > 2 0 49

echo "Set target to ${TARGET_VOLT}:"
pigs spix 0 0x2 ${TARGET_VOLT}

echo "Read voltage from AD:"
pigs spix 0 0x83 0

pigs spic 0 # close spi 0


```

__________________________________________________________________

## SPI control

This device uses 16 bit words divided in an 8bit Address 
and 8 bit data. Address bit 7 is used to signal a read 
request.


| Adr   | Function       | R/W | Description             | 
|-------|----------------|-----|-------------------------|
| 0x00  | NOP            | R/W | No operation            |
| 0x01  | SPI ID         |  R  | Will return 49 or 0x31  |
| 0x02  | Voltage target | R/W | Set the PID target volt.|
| 0x03  | Voltage AD     |  R  | Reads indicated voltage |
| 0x04  | AD Dir         |  W  | Flips the ADC direction |
| 0x20  | K-value P      | R/W | Set K, gain factor      |
| 0x21  | K-value I      | R/W | Set K, gain factor      |
| 0x22  | K-value D      | R/W | Set K, gain factor      |


__________________________________________________________________

## Build environment setup:
Configure your dev environment, this was tested on Ubuntu LTS 19.
Note that this project is set up to program the attiny85 using an 
avr dragon board.

### Tool chain, compiler and tools
For most avr chips, the prebuilt GCC and toolchain from aptitude 
is sufficient. 

Install the compiler and avrdude:
```
sudo apt-get install gcc-avr
sudo apt-get install avrdude
sudo apt-get install avr-libc
```

Also, the makefiles in this project are written for cmake:
```
sudo apt install cmake
```
This project should be fully compatible with the CLion IDE

### Permissions
It's a good idea to be a member of the "dialout" group on your 
system since you will probably want to program your device without 
having to use sudo.

If you are not sure about this, just run:
```
sudo adduser $USER dialout
```

### Headers

If you want to browse the avr header files they can most likely be 
found in:
```
/usr/lib/avr/include/
```

If this is not the case on your machine, try searching /usr for a 
known header file:
```
find /usr -name iotn85.h
```

__________________________________________________________________

## Change AVR device
Note that the code is written for an attiny85, don't expect 
everything to work on different hardware.

If you still want to change to a different AVR chip, a few 
tweaks are needed:

### CMakeLists.txt:
Change the following line in CMakeLists.txt to match your device:
```
SET(CMCU "-mmcu=attiny85")
```
Also remember to set the target frequency for your device like so:
```
SET(CDEFS "-DF_CPU=16000000")
```

### programTarget.sh
Change the target device for avrdude.

You can list all available devices using:
```
avrdude -p ?
```
