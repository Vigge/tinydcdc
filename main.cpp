
#include <avr/io.h>
#include <stdlib.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <string.h>
#include "pwm.h"
#include "adc.h"
#include "pidDcDc.h"
#include "spislaveusi.h"
#include "ssfir.h"

/***********************************************************
 * TinyDcDC                                                *
 * - A small spi controlled DC/DC module based on attiny85 *
 *                                                         *
 * See README.md for additional info and use cases         *
 ***********************************************************
 *
 */

// Init static driver objects:
static Spislaveusi spi = Spislaveusi();
static Pwm pwm = Pwm();
static Adc adc = Adc(Adc::PIN_ADC2);
PidDcDc pid = PidDcDc();

// Constants and Globals:
static const uint16_t chipID = 0x31;
static int16_t freeRamCached = 0;

int16_t freeRam () {
    extern int __heap_start, *__brkval;
    int16_t v;
    return (int16_t) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
}

//void blink()
//{
//    DDRB |= (1 << PB3); // status led for debug
//    PORTB |= (1 << PB3);
//    _delay_ms(150);
//    PORTB &= ~(1 << PB3);
//    _delay_ms(150);
//    PORTB |= (1 << PB3);
//}
//
//void led1(bool setLedOn)
//{
//    if (setLedOn)
//        PORTB |= (1 << PB1);
//    else
//        PORTB &= ~(1 << PB1);
//}
//
//void inline led3(bool setLedOn)
//{
//    if (setLedOn)
//        PORTB |= (1 << PB3);
//    else
//        PORTB &= ~(1 << PB3);
//}

ISR(SIG_USI_OVERFLOW)
{
    spi.updateStateMachine();

    uint8_t addr = spi.processReadRequest();
    // Handle SPI read
    switch (addr)
    {
        case 0x1:
            spi.returnData(chipID);
            break;
        case 0x2:
            spi.returnData((pid.getTarget() / 4)); // 10 bit adc into 8bit control data
            break;
//        case 0x3:
//            spi.returnData(test2);

        case 0x60:  // Debug, 0xE0 will check free ram and return MSB
            freeRamCached = freeRam();
            spi.returnData(static_cast<uint8_t>(freeRamCached >> 8));
            break;
        case 0x61:  // Debug, 0xE1 will return the LSB of free ram from 0xE0 check
            spi.returnData(static_cast<uint8_t>(0xFF & freeRamCached));
            break;
        default:
            // do nothing, no read request ongoing
            break;
    }

    // Handle SPI write
    uint8_t data = 0;
    addr = spi.processWriteRequest(&data);
    switch (addr)
    {
        case 0x2:
            pid.setTarget(static_cast<uint16_t>(data * 4)); // 8bit control data into 10 bit adc
            break;
        case 0x4:
            adc.flipAdcDir();
            break;
        default:
            // do nothing, no write request ongoing
            break;
    }
}

int main() {
    bool run = true;
    bool ledStatus = false;

//    int16_t count = 0;
//    int16_t target = 300;
//    int16_t targetHi = 500;
//    int16_t targetLo = 300;

//    DDRB |= (1 << PB3); // Set PB3 to output for LED ind (debugging)
//    PORTB = 0;
//    USIBR = 1;

//    MCUCR &= ~(1 << PUD); // Global pull up disable
    sei();

//    adc.flipAdcDir();   // Use adc in reverse mode
    pid.setTarget(40);  // Default target

    // Start of program indicator
//    blink();
//    blink();

//
    // Set default K values (for a simple RC filtered PWM)
    pid.setKFactorP(247);
    pid.setKFactorI(185);
    pid.setKFactorD(245);


//
//    pid.setTarget(targetLo);
//
//    pwm.setPwmRatio(0xFF); // min pwm for 5ms, use for triggering init on an oscilloscope
//    _delay_ms(5);
//
    uint16_t adcVal;
    while (run)
    {
        // Regulate the voltage indefinitely:
        adcVal = adc.readResultBlocking10();
        adc.startCapture();

        // Do the pid calculations while the ADC is sampling:
        pid.tick16bit(adcVal);

        pwm.setPwmRatio(pid.getPwmVal8bit());

        // End of DC/DC reg.

        /* Debug:
         * Toggle PB1 to allow measuring the reg loop freq
         * Run a PID calibration program (Can be used with an oscilloscope to trim K-values)
         */


//        pwm.setPwmRatio(adcVal / 4);
//        ledStatus = !ledStatus;
//        led3(ledStatus);




    }

    return 0;
}