#include "spislaveusi.h"

Spislaveusi::Spislaveusi()
{
    // Set USI to 3-wire and external clock (Slave mode):
    // Enable SIG_USI_OVERFLOW interrupt with USIOIE
    USICR = (1 << USIWM0) | (1 << USICS1) | (1 << USIOIE);

    //invert clock
//    USICR |= (1 << USICS0);

    DDRB |= (1 << PB1); // Set PB1 to output (MISO/DO pin)
}

uint8_t Spislaveusi::readDataByte()
{
    return buf[spiState - 2];
}

//uint8_t Spislaveusi::readByteBlocking()
//{
//    // Clear counter overflow flag
//    USISR |= (1<<USIOIF);
//
//    while ((USISR & (1<<USIOIF))==0) {
//        ;
//    }
//
//    uint8_t tmp = USIDR;
//    USIDR = 0;
//
//    return USIDR;
//}

void Spislaveusi::writeByte(uint8_t data)
{
    USIDR = data;
}

void Spislaveusi::updateStateMachine()
{
    // Clear counter overflow flag
    USISR |= (1<<USIOIF);


    if (spiState == SPI_STATE::NEXTBYTE_ADDR)
    {
        cachedAddr = USIDR;
    }
    else if (spiState >= SPI_STATE::NEXTBYTE_DATA0 && spiState <= SPI_STATE::SPI_STATE_END)
    {
        if (buffDirOutput && spiState < SPI_STATE::SPI_STATE_END - 1) // Write data from buffer
        {
            // Note, when the addr byte has been received, returnData() will write the first byte.
            // returnData() will write the first byte: index -> spiState - 1
            writeByte(buf[spiState - 1]);
        }
        else // Receive data in to buffer
        {
            buf[spiState - 1] = USIDR; // Addr is stored in its own variable -> spiState - 1
        }
    }

    if (!buffDirOutput && !(cachedAddr & 0x80))
    {
        writeByte(0); // No read request received, return 0 as next byte.
    }

    // Advance word state machine:
    spiState = static_cast<SPI_STATE>(static_cast<uint8_t>(spiState) + 1);
    if (spiState >= SPI_STATE::SPI_STATE_END) // End of word
    {
        buffDirOutput = false;
        doWriteRequest = true;
        spiState = SPI_STATE::NEXTBYTE_ADDR;
    }
}

uint8_t Spislaveusi::processReadRequest()
{
    if (cachedAddr & 0x80)
    {
        uint8_t tmp = cachedAddr & 0x7F;
        cachedAddr = 0; // Clear read request
        return tmp;
    }

    return 0;
}

#ifdef SPI_24B_WORD
void Spislaveusi::returnData(uint16_t data)
{
    buffDirOutput = true;
    // Data is stored little endian
    // Output MSB on SPI first, buffer LSB (Write later in state machine loop)
    // MSB is the low byte (AVR and GCC generally little endian)
    writeByte(static_cast<uint8_t>(data >> 8));
    buf[0] = static_cast<uint8_t>(0xFF & data);
}
#else
void Spislaveusi::returnData(uint8_t data)
{
    buffDirOutput = true;
    writeByte(data);
}
#endif

#ifdef SPI_24B_WORD
uint8_t Spislaveusi::processWriteRequest(uint16_t *data)
{
    if (doWriteRequest)
    {
        doWriteRequest = false;
        // Store whats in the big endian buffer as little endian uint16_t:
        *data = buf[1];
        *data |= buf[0] << 8;
        return cachedAddr;
    }

    return 0;
}
#else
uint8_t Spislaveusi::processWriteRequest(uint8_t *data)
{
    if (doWriteRequest)
    {
        doWriteRequest = false;
        *data = buf[0];
        return cachedAddr;
    }

    return 0;
}
#endif
