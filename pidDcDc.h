//
// Created by vigge on 22/07/19.
//

#ifndef TINYDCDC_PIDDCDC_H
#define TINYDCDC_PIDDCDC_H

#define INTERRUPSAFE 1

#ifdef INTERRUPSAFE
#include <avr/interrupt.h>
#endif
#include <stdint-gcc.h>
//#include "ssfir.h"

class PidDcDc
{
public:
    PidDcDc();

    void tick16bit(uint16_t feedback);

    uint8_t getPwmVal8bit();

    void setTarget(uint16_t target);
    uint16_t getTarget();

    /*
     * Sets the K factors of the regulator.
     * These need to be trimmed for your the specific application.
     */
    void setKFactorP(uint8_t factor);
    void setKFactorI(uint8_t factor);
    void setKFactorD(uint8_t factor);

private:
    bool firstTick = true;
    int16_t target;
    uint8_t outputVal = 0xFF;
    int16_t errorSum = 0;
    int16_t lastProcessValue = 0;

//    Ssfir fir = Ssfir(1);

    /*
     * // K values - PID gain
     * Note! The K values are stored in a flipped state here!
     *
     * These are all denominators since float factors would slow down the code too much.
     * - 1 is the maximum gain
     * - zero = off
     *
     * These default values work decent for a simple RC filtered PWM with R=680 ohm C=100 nF.
     * Use the setKFactor functions to trim these for your application.
     */
    uint8_t kP = 8;
    uint8_t kI = 70;
    uint8_t kD = 10;

    /* These work decent for a simple RC filtered PWM. R=680 ohm C=100 nF
     * pid.setKFactorP(247); -> kP = 8;
     * pid.setKFactorI(185); -> kI = 70;
     * pid.setKFactorD(245); -> kD = 10;
     */

};


#endif //TINYDCDC_PIDDCDC_H
