#include <stdlib.h>
#include "ssfir.h"

Ssfir::Ssfir(uint8_t bufferSize)
{
    // Note, only uint8_t since that could probably fill most of the ram of any 8-bit AVR

    bufSize = bufferSize;
    /* Note, use of malloc here as the avr compiler will add a significant amount of
     * program mem bytes for calloc (about 106 bytes compared to 18 for the same malloc op)
     * Buffer initialization can be done in fewer bytes with the setFilterInitCondition()
     * function!
     */
    buf = (int16_t*)malloc(bufSize * sizeof(int16_t));
    if (buf == NULL)
        err |= 1;   // Out of memory!

}
Ssfir::~Ssfir()
{
    if (buf)
        free(buf);
}

int16_t Ssfir::lowPassSample()
{
    // A simple all buffer average, we expect the buffer to always be full!
    uint32_t sum = 0;

    for (int i = 0; i < bufSize; i++)
    {
        sum += buf[i];
    }
    return sum / bufSize;
}

int16_t Ssfir::highPassSample()
{
    return buf[tail] - lowPassSample();
}

void Ssfir::setFilterInitCondition(int16_t initValue)
{
    bufferInitialized = true;
    for (int i = 0; i < bufSize; i++)
    {
        addSample(initValue);
    }
}

void Ssfir::addSample(int16_t sample)
{
    // If init conditions are not already specified, lazy init to the first sample!
    if (!bufferInitialized)
        setFilterInitCondition(sample);

    buf[head] = sample;
    advanceHead();
}

// Uncomment if you need this (Adds around 86 Bytes of progmem)
//int16_t Ssfir::popSample()
//{
//    if (!isBufEmpty())
//    {
//        uint8_t tmp = tail;
//        advanceTail();
//        return buf[tmp];
//    }
//
//    err |= 2;
//    return 0;
//}

uint8_t Ssfir::checkErrors()
{
    // See list of error codes in .h file!
    return err;
}

bool inline Ssfir::isBufEmpty()
{
    return head == tail;
}

void inline Ssfir::advanceHead()
{
    head += 1;
    if (head >= bufSize)
        head = 0;

    if (head == tail)   // the buffer was full, last value just got cleared
        advanceTail();

}

void inline Ssfir::advanceTail()
{
    tail += 1;
    if (tail >= bufSize)
        tail = 0;
}
