///
/// SPI slave driver for attiny with USI hardware
///
/// Features:
/// Interrupt driven
/// 16 or 24 bit words with 8bit address and 8-bit or 16-bit data
/// Addr bit 7 indicates read operation
///
/// Usage:
/// Implement the SIG_USI_OVERFLOW interrupt handling in your code.
/// * updateStateMachine()   manages all spi state operation for you such as addr/data bit and buffer.
/// * processReadRequest()   Outputs the address the master is requesting to read.
/// * returnData()           Is used to respond the master device.
/// * processWriteRequest()  Ouputs the address and data the master device requests to write.
///
///
/// Example code, 16bit mode:
///
/// #include <avr/io.h>
/// #include <stdlib.h>
/// #include <avr/interrupt.h>
///
/// static Spislaveusi spi = Spislaveusi();
/// static uint8_t aValue = 0;
/// static const uint8_t chipID = 0x99;
///
/// ISR(SIG_USI_OVERFLOW)
/// {
///     spi.updateStateMachine();
///
///     uint8_t addr = spi.processReadRequest();
///     // Handle SPI read (read bit 7 is masked in processReadRequest())
///     switch (addr)
///     {
///         case 0x1:
///             spi.returnData(chipID); // Returns chipID to the master as data part of the spi word
///         break;
///
///         default:
///             // do nothing, no read request ongoing
///             break;
///     }
///
///     // Handle SPI write
///     uint8_t data = 0;
///     addr = spi.processWriteRequest(&data);
///     switch (addr)
///     {
///         case 0x2:
///             aValue = data; // sets aValue to a value from the master device
///         break;
///
///         default:
///             // do nothing, no write request ongoing
///             break;
///     }
/// }
///
///

#ifndef TINYDCDC_SPISLAVEUSI_H
#define TINYDCDC_SPISLAVEUSI_H

#include <avr/io.h>
#include <stdlib.h>
#include <avr/interrupt.h>

//#define SPI_24B_WORD 1    // uncomment for 24-bit mode
#define NUM_DATA_BYTES 2    // Mostly affects the sw buffer. Use at least 2 for 24-bit mode

class Spislaveusi
{
    enum SPI_STATE
    {
        NEXTBYTE_ADDR = 0,   // Next byte received sets the address to read or write. Always used
        NEXTBYTE_DATA0, // Next byte received is data. Always used
#ifdef SPI_24B_WORD
        NEXTBYTE_DATA1,  // Next byte received is data. Used in 24-bit mode
#endif
        SPI_STATE_END
    };

public:
    Spislaveusi();

    // Reads a byte from the software buffer
    uint8_t readDataByte();

    // This can be used for debug otherwise this implementation is made to work with interrupts only.
//    uint8_t readByteBlocking();

    // Write a byte to buffer to be received on the master side after 8 clock cycles
    void writeByte(uint8_t data);

    /* returnData:
     *
     * Used to write response data back to the master
     */
#ifdef SPI_24B_WORD
    void returnData(uint16_t data);
#else
    void returnData(uint8_t data);
#endif

    /* updateStateMachine:
     *
     * Manages all spi state logic, call once per SIG_USI_OVERFLOW
     */
    void updateStateMachine();

    /* processReadRequest:
     *
     * If a read request is ongoing:
     *   - Returns the address for the request
     * No read requests are ongoing:
     *   - Returns zero
     */
    uint8_t processReadRequest();


    /* processWriteRequest:
     *
     * If write request received:
     *   - Returns the address and associated data for the request
     * If no write request are ongoing:
     *   - Returns zero
     */
#ifdef SPI_24B_WORD
    uint8_t processWriteRequest(uint16_t *data);
#else
    uint8_t processWriteRequest(uint8_t *data);

#endif

private:
    uint8_t buf[NUM_DATA_BYTES];
    uint8_t cachedAddr = 0;

    // SPI State machine:
    SPI_STATE spiState = NEXTBYTE_ADDR;
    bool buffDirOutput = false; // true: Write data from buffer, false: receive in buffer
    bool doWriteRequest = false;   // When data has been received, this triggers a writeRequest
};

#endif //TINYDCDC_SPISLAVEUSI_H
