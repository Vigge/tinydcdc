#!/bin/bash
pwd

EXECNAME="tinyDcDc"
BINDIR=$(pwd)/cmake-build-debug

./build.sh || exit 1

./packHexFile.sh ${EXECNAME} ${BINDIR} || exit 1

avrdude -p t85 -B 1MHz -c dragon_isp -P usb -e -U flash:w:${BINDIR}/${EXECNAME}.hex  # User should be part of "dialout" group

