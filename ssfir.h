//
// Super Simple FIR filter
//
// - Uses a ring sample buffer
// - 1 to 255 samples of filtering
// - Rolloff from Nyquist (1 sample) to Nyquist / 1020 (255 samples)
// - Init conditions support (Fills the buffer with a predetermined value or the first sample)
// - Uses 16-bit samples ( 2 bytes RAM per sample )
// - Note that many avr devices only have 512 bytes of ram. The sample buffer can fill it very quickly!
// - The whole implementation takes around 1KB of program memory in (-Os)
//
// Usage:
// Select a frequency at init using the bufferSize. bufferSize = Fs / 2Ff
// were Fs is the sample frequency and Ff the desired filter frequency.
//
// Example: Fs = 2 KHz, Ff = 125 KHz -> bufferSize = 8
// > Ssfir firFilter = Ssfir(8);
//
// In your sampling loop/interrupt, add samples to Ssfir:
// > firFilter.addSample(getNextAdcSample());
//
// Get filtered samples back in the output loop using the sample functions:
// > sendToDac(firFilter.lowPassSample());
//

#ifndef TINYDCDC_SSFIR_H
#define TINYDCDC_SSFIR_H


#include <stdint-gcc.h>

class Ssfir
{
public:
    Ssfir(uint8_t bufferSize);
    ~Ssfir();

    void addSample(int16_t sample);

    /*
     * Pops the last sample from the buffer.
     *
     * Uncomment if you need it, uses ~86 Bytes program memory
     */
//    int16_t popSample();

    /* Check error codes:
     * 0    OK!
     * 1    Out of memory
     * 2    Couldn't pop from empty buffer
     */
    uint8_t checkErrors();

    /*
     * Returns a filtered sample from the buffer.
     */
    int16_t lowPassSample();      // ~168 Bytes program memory
    int16_t highPassSample();     //  ~42 Bytes program memory

    /*
     * Initializes the buffer to a certain start condition.
     *
     * uses ~56 Bytes program memory
     */
    void setFilterInitCondition(int16_t initValue);

private:
    uint8_t err = 0;
    int16_t* buf;
    uint8_t bufSize;
    bool bufferInitialized = false;

    uint8_t head = 0;
    uint8_t tail = 0;

    bool inline isBufEmpty();
    void inline advanceHead();
    void inline advanceTail();

};


#endif //TINYDCDC_SSFIR_H
