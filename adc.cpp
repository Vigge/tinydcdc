#include <avr/io.h>
#include <util/delay.h>
#include "adc.h"

Adc::Adc(Adc::ADC_PIN pin)
{
    /*
     * ADMUX:
     *   7     6     5     4    3    2    1    0
     * REFS1 REFS0 ADLAR REFS2 MUX3 MUX2 MUX1 MUX0
     */

    // REFS[2:0] - Voltage Reference Selection Bits: 00 internal ref from Vcc
    // ADLAR: 1 left adjust (Easy read of 8 bits via ADCH reg)
    // REFS2: 0 internal ref from Vcc
    // MUX, handled by

    ADMUX = 0b00100000;
    initIO(pin);

    /*
     * ADC prescale.
     * full scale -> 50KHz < ADCClk < 200KHz
     * Recommended max ADCClk: 1MHz
     *
     * This project uses a 16MHz system clock, CK/128 -> 125KHz
     * NOTE: adjust pollCaptureDone timing accordingly!
     *
     * ADCSRA:
     *   7   6     5     4   3     2     1     0
     * ADEN ADSC ADATE ADIF ADIE ADPS2 ADPS1 ADPS0
     */

    ADCSRA = 0b11010111;

}

static bool pollCaptureDone()
{
    for (uint8_t i = 0; i < 255; i++)
    {
        if ((ADCSRA & (1 << ADIF)) != 0)
        {
            return true;
        }

        /*
         * - Wait 1275 us + loop clock cycles max. else fail.
         * - One ADC conversion takes about 8 * 13 us with a 125KHz clock
         *
         * If called directly after capture start, this loop seems to exit around iteration 31. (~6KHz)
         */

        _delay_us(5);
    }

    return false;
}

void Adc::initIO(Adc::ADC_PIN pin)
{
    ADMUX &= 0xF0;  // Reset the mux;
    ADMUX |= pin;

    // Set DDR to input and disable pull up:
    // mux 0, PB5
    // mux 1, PB2
    // mux 2, PB4
    // mux 3, PB3
    uint8_t mask;
    switch (pin)
    {
        case Adc::PIN_ADC0:
            mask = 1 << PB5;
            break;
        case Adc::PIN_ADC1:
            mask = 1 << PB2;
            break;
        case Adc::PIN_ADC2:
            mask = 1 << PB4;
            break;
        case Adc::PIN_ADC3:
            mask = 1 << PB3;
            break;
        default:
            mask = 0;
    }
    DDRB &= ~mask;
    PORTB &= ~mask;
}

void Adc::flipAdcDir()
{
    flipDir = !flipDir;
}

void Adc::startCapture()
{
    // ADSC -> 1: start a new conversion
    // ADIF -> 1: clear conversion complete bit
    ADCSRA |= (1 << ADSC) | (1 << ADIF);
}

uint16_t Adc::readResultBlocking10()
{
    uint16_t flipVal = flipDir ? 0x3FF : 0;
    pollCaptureDone();
    return flipVal - ((ADCL >> 6) | (ADCH << 2));
}

uint8_t Adc::readResultBlocking8()
{
    uint8_t flipVal = flipDir ? 0xFF : 0;
    pollCaptureDone();
    return flipDir - ADCH;
}
