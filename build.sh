#!/bin/bash

cmake --build cmake-build-debug --clean-first --target all

#Optional, display device flash info with avr-size. attiny85 has 8Kb program flash
avr-size cmake-build-debug/tinyDcDc.elf --format=avr --mcu=attiny85