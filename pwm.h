//
// Created by vigge on 15/07/19.
//

#ifndef TINYDCDC_PWM_H
#define TINYDCDC_PWM_H

#include <avr/io.h>

class Pwm {

public:
/*
 * Freq table:
CS13  CS12  CS11  CS10 Async. mode sync. mode
0      0     0     0      stop       stop
0      0     0     1      PCK        CK
0      0     1     0      PCK/2      CK/ 2
0      0     1     1      PCK/4      CK/4
0      1     0     0      PCK/8      CK/8
0      1     0     1      PCK/16      CK/16
0      1     1     0      PCK/32      CK/32
0      1     1     1      PCK/64      CK/64
1      0     0     0      PCK/128      CK/128
1      0     0     1      PCK/256        CK/256
1      0     1     0      PCK/512      CK/512
1      0     1     1      PCK/1024      CK/1024
1      1     0     0      PCK/2048      CK/2048
1      1     0     1      PCK/4096      CK/4096
1      1     1     0      PCK/8192      CK/8192
1      1     1     1      PCK/16384      CK/16384
*/
    enum FREQ // Possible freq divisions for attiny85
    {
        STOP      = 0,
        PCK       = 0b1,
        PCK_2     = 0b10,
        PCK_4     = 0b11,
        PCK_8     = 0b100,
        PCK_16    = 0b101,
        PCK_32    = 0b110,
        PCK_64    = 0b111,
        PCK_128   = 0b1000,
        PCK_256   = 0b1001,
        PCK_512   = 0b1010,
        PCK_1024  = 0b1011,
        PCK_2048  = 0b1100,
        PCK_4096  = 0b1101,
        PCK_8192  = 0b1110,
        PCK_16384 = 0b1111,
    };

    Pwm();

    void setPwmRatio(char percent);

};


#endif //TINYDCDC_PWM_H
