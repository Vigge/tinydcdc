#include "pidDcDc.h"

PidDcDc::PidDcDc()
{

}

static int16_t safeAdd(int16_t a, int16_t b, int16_t max, int16_t min)
{
    int16_t sum = a + b;

    if (a >= 0 && b >= 0)
    {
        if (sum < 0)    // 16 bit overflow occurred, return max val;
            return 0x7FFF;
    }

    if (a < 0 && b < 0)
    {
        if (sum > 0)    // 16 bit overflow occurred, return min val;
            return -0x8000;
    }
    return sum;
}

static uint8_t addAndClamp(int16_t a, int16_t b)
{
    int16_t sum = a + b;

    if (sum > 0xFF)
        return 0xFF;
    else if (sum <= 0)
        return 0;

    return sum;
}

static int16_t addAndClamp(int16_t a, int16_t b, int16_t max, int16_t min)
{
    int16_t sum = a + b;

    if (sum > max)
        return max;
    else if (sum <= min)
        return min;

    return sum;
}

void PidDcDc::tick16bit(uint16_t processValue)
{
    if (firstTick)
    {
//        fir.setFilterInitCondition(processValue);
        lastProcessValue = processValue;
        firstTick = false;
    }

    int16_t error = (target - static_cast<int16_t>(processValue));

    // pP Proportional part (P)
    int16_t pP = 0;
    if (kP != 0)
    {
        pP = error / kP;
    }

    // pI Integral part (I)
    int16_t pI = 0;
    if (kP != 0)
    {
        errorSum = addAndClamp(errorSum, error, 256, -256);
        pI = errorSum / kI;
    }

    // pD Derivative part (D)
    int16_t pD = 0;
    if (kD != 0)
    {
//        int16_t oldFilteredProcessValue = fir.lowPassSample();
//        fir.addSample(processValue);
//        int16_t filteredProcessValue = fir.lowPassSample();
//        pD = (oldFilteredProcessValue - filteredProcessValue) / kD;

        pD = (lastProcessValue - static_cast<int16_t>(processValue)) / kD;
    }

    lastProcessValue = static_cast<int16_t>(processValue);

    outputVal = addAndClamp(outputVal, pP);
    outputVal = addAndClamp(outputVal, pI);
    outputVal = addAndClamp(outputVal, pD);
}

uint8_t PidDcDc::getPwmVal8bit()
{
    return outputVal;
}

void PidDcDc::setTarget(uint16_t target)
{
#ifdef INTERRUPSAFE
    cli();
#endif

    this->target = static_cast<int16_t>(target);

#ifdef INTERRUPSAFE
    sei();
#endif
}

uint16_t PidDcDc::getTarget()
{
#ifdef INTERRUPSAFE
    cli();
#endif

    uint16_t output = static_cast<uint16_t>(this->target);

#ifdef INTERRUPSAFE
    sei();
#endif
    return output;
}

void PidDcDc::setKFactorP(uint8_t factor)
{
    if (factor == 0)
        kP = 0;
    else
        kP = 0xFF - factor;
}

void PidDcDc::setKFactorI(uint8_t factor)
{
    if (factor == 0)
        kI = 0;
    else
        kI = 0xFF - factor;
}

void PidDcDc::setKFactorD(uint8_t factor)
{
    if (factor == 0)
        kD = 0;
    else
        kD = 0xFF - factor;
}
