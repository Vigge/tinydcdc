/****************************************
 * Single ended ADC driver for ATTiny85 *
 ***************************************/


#ifndef TINYDCDC_ADC_H
#define TINYDCDC_ADC_H


#include <stdint-gcc.h>

class Adc {
public:
    // Input channel selection: (See mux table 17-4 from tiny85 datasheet)
    enum ADC_PIN
    {
        PIN_ADC0 = 0,   // mux 0, PB5
        PIN_ADC1 = 1,   // mux 1, PB2
        PIN_ADC2 = 2,   // mux 2, PB4
        PIN_ADC3 = 3    // mux 3, PB3
    };

    Adc(Adc::ADC_PIN pin);

    void initIO(ADC_PIN pin);

    void flipAdcDir();

    void startCapture();
    uint16_t readResultBlocking10();
    uint8_t readResultBlocking8();

private:
    bool flipDir = false;

};


#endif //TINYDCDC_ADC_H
