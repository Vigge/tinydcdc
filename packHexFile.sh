#!/bin/bash
# Strips and repacks a gnu elf file into a hex that can be used by AVRs
EXECNAME="$1"
BINDIR="$2"

echo "Repacking executable..."

echo "Executable: $(ls -lh ${BINDIR}/${EXECNAME}.elf)"
echo "Executable md5sum: $(md5sum ${BINDIR}/${EXECNAME}.elf)"

avr-objcopy -j .text -j .data -O ihex ${BINDIR}/${EXECNAME}.elf ${BINDIR}/${EXECNAME}.hex
echo "Hex: $(ls -lh ${BINDIR}/${EXECNAME}.hex)"
echo "Hex md5sum: $(md5sum ${BINDIR}/${EXECNAME}.hex)"
